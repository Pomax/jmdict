import lookup from "./ime.js";
import sorting from "./utils.js";
const { convert } = lookup;

function create(tag, content, props = {}) {
  const e = document.createElement(tag);
  e.textContent = content;
  Object.entries(props).forEach(([key, val]) => (e[key] = val));
  return e;
}

const radicals = await fetch(`radicals.json`).then((v) => v.json());
const kanaRange = /[\u3040-\u30FF]/u;
const kanjiRange = /[\u3300-\u33FF\u3400-\u4DBF\u4E00-\u9FFF\uF900-\uFAFF]/u;

function getRadical(num) {
  if (!num.some) return getRadical([num]);
  return num.map((n) => {
    let r = radicals.find((r) => r.id === `${n}`);
    if (!r) {
      r = radicals.find((r) => r.id === `${n}.1`);
    }
    return [n, r.glyph];
  });
}

function romanise(reading, label = reading, suffix) {
  if (suffix) {
    return `<ruby><a href=".?jpen=${reading}">${label}<span class="suffix">${suffix}</span></a><rt>${
      convert(label).romaji
    }<span class="suffix">${convert(suffix).romaji}</span></rt></ruby>`;
  }
  return `<ruby><a href=".?jpen=${reading
    .replaceAll(`-`, `*`)
    .replace(/[^\u3040-\u30FFー*]/g, ``)}">${label}</a><rt>${
    convert(reading).romaji
  }</rt></ruby>`;
}

/**
 * ...
 * @param {*} entry
 * @returns
 */
function kanjiEntry(entry) {
  const { literal, radical, reading_meaning, misc } = entry;
  const { stroke_count, jlpt } = misc;
  const { reading, meaning } = reading_meaning.rmgroup;
  const div = create(`div`);
  div.classList.add(`kanji-entry`);
  const onyomi = reading
    .filter((v) => v.match(/[^ぁ-ゖー\.\-]/))
    .map((v) => romanise(v))
    .join(`, `);
  const kunyomi = reading
    .filter((v) => v.match(/[^ァ-ヺー\.\-]/))
    .map((v) => {
      if (!v.includes(`.`)) {
        return romanise(v);
      }
      const term = v.replaceAll(`-`, `*`).replace(/[^ぁ-ゖー*]/g, ``);
      const yomi = v.substring(0, v.indexOf(`.`));
      const suffix = v.substring(v.indexOf(`.`) + 1);
      return romanise(term, yomi, suffix);
    })
    .join(`, `);
  const codepoint = literal.codePointAt(0).toString(16);
  const rad = getRadical(radical.rad_value)
    .map(([n, r]) => `<a href="?kanji=${r}">${r} (${n})</a>`)
    .join(`, `);

  const jlptNum = entry.misc.jlpt ? `jlpt ${entry.misc.jlpt},` : ``;

  div.innerHTML = `
    <p><img alt="${literal}"src="100/${codepoint.substring(
    0,
    2
  )}/${codepoint}-100pt.gif"></p>
    <p>${literal}, ${stroke_count} strokes, ${jlptNum} indexed by ${rad}</p>
    <p>Onyomi: ${onyomi}</p>
    <p>Kunyomi: ${kunyomi}</p>
    <p>Meaning: ${meaning.join(`, `)}</p>
    <p><a href="./?jpen=*${literal}*">View all words that contain this kanji</a></p>
  `;
  return div;
}

/**
 * ...
 * @param {*} entry
 * @returns
 */
function jpenEntry(entry) {
  const div = create(`div`);

  div.classList.add(`jpen-entry`);
  div.dataset.reb = entry.r_ele[0].reb;
  div.dataset.rom = convert(div.dataset.reb).romaji;
  div.dataset.keb =
    entry.k_ele.find(({ keb }) => keb.match(kanjiRange))?.keb ?? `x`;

  const k_ele = entry.k_ele
    .map((k) => {
      return `<span>${k.keb
        .split(``)
        .map((c) => {
          if (c.match(kanjiRange)) {
            return `<a href="./?kanji=${c}">${c}</a>`;
          }
          return c;
        })
        .join(``)}</span>`;
    })
    .join(`, `);

  div.innerHTML = `
    <p>${k_ele}</p>
    <p>${entry.r_ele.map((r) => romanise(r.reb)).join(`, `)}</p>
    <ul>${entry.sense
      .map(
        (s) =>
          `<li>(<span>${s.pos
            .sort()
            .map((v) => v.substring(1, v.length - 1))
            .join(`, `)}</span>) <span>${s.gloss.join(`, `)}</span></li>`
      )
      .join(``)}</ul>
  `;

  return div;
}

/**
 * ...
 * @param {*} entry
 * @returns
 */
function giongoEntry(entry) {
  const div = create(`div`);
  div.classList.add(`giongo-entry`);
  div.innerHTML = `
  <p>${romanise(entry.reb)}</p>
  <blockquote>
  ${entry.sense
    .map((s) => {
      return `<p>${s.type}</p><ul>${s.meaning
        .map((m) => {
          return `<li>${m}</li>`;
        })
        .join(`\n`)}</ul>`;
    })
    .join(`\n`)}
  </blockquote>`;
  return div;
}

/**
 * ...
 * @param {*} type
 * @param {*} term
 * @param {*} toHTML
 */
function search(type, term, toHTML) {
  const url = `?${type}=${encodeURIComponent(term)}`;
  if (url !== location.search) {
    history.pushState(null, ``, url);
  }
  fetch(`api/v1/${type}/${term}`)
    .then((res) => res.json())
    .then((data) => {
      results.innerHTML = ``;
      const { english, japanese } = data;

      const rcount = english.length + japanese.length;

      if (rcount === 0) {
        return (results.textContent = `No results.`);
      }

      resultCount.textContent = `${rcount} Result${
        resultCount === 1 ? `` : `s`
      }`;

      if (english.length > 0) {
        const en = create(`h1`, `English`);
        const enres = create(`div`, ``, { id: `enres` });
        results.appendChild(en);
        results.appendChild(enres);

        english.forEach((entry) => enres.appendChild(toHTML(entry)));

        // sort english by romaji
        const list = Array.from(enres.querySelectorAll(`.jpen-entry`));
        list.sort(sorting.romaji).forEach((e) => enres.appendChild(e));
      }

      if (japanese.length > 0) {
        const jp = create(`h1`, `Japanese`);
        const jpres = create(`div`, ``, { id: `jpres` });
        results.appendChild(jp);
        results.appendChild(jpres);
        japanese.forEach((entry) => jpres.appendChild(toHTML(entry)));

        // sort japanese by kana
        const list = Array.from(jpres.querySelectorAll(`.jpen-entry`));
        list.sort(sorting.reading).forEach((e) => jpres.appendChild(e));
      }
    });
}

const kinput = document.querySelector(`input.kanji`);
const ksearch = () => search(`kanji`, kinput.value, kanjiEntry);
kinput.addEventListener(`keyup`, ({ key }) => key === `Enter` && ksearch());
kanji.addEventListener(`click`, ksearch);

const jinput = document.querySelector(`input.jpen`);
const jsearch = () => search(`jpen`, jinput.value, jpenEntry);
jinput.addEventListener(`keyup`, ({ key }) => key === `Enter` && jsearch());
jpen.addEventListener(`click`, () => search(`jpen`, jinput.value, jpenEntry));

const ginput = document.querySelector(`input.giongo`);
const gsearch = () => search(`giongo`, ginput.value, giongoEntry);
ginput.addEventListener(`keyup`, ({ key }) => key === `Enter` && gsearch());
giongo.addEventListener(`click`, () =>
  search(`giongo`, ginput.value, giongoEntry)
);

const s = window.location.search;
const params = new URLSearchParams(s.substring(1));

const jterm = params.get(`jpen`);
if (jterm) {
  jinput.value = jterm;
  jpen.click();
}

const kterm = params.get(`kanji`);
if (kterm) {
  kinput.value = kterm;
  kanji.click();
}

const gterm = params.get(`giongo`);
if (gterm) {
  ginput.value = gterm;
  giongo.click();
}

// filtering
const filters = {
  romaji: document.querySelector(`#romaji`),
  // sorting: document.querySelector(`#sorting`),
  // ascdesc: document.querySelector(`#ascdesc`),
};

filters.romaji.addEventListener(`change`, (evt) => {
  const style = filters.romaji.nextElementSibling;
  if (!filters.romaji.checked) {
    style.removeAttribute(`type`);
  } else {
    style.setAttribute(`type`, `inactive`);
  }
});

// filters.sorting.addEventListener(`change`, (evt) => {
//   const { value } = filters.sorting;
//   console.log(value);
// });

// filters.ascdesc.addEventListener(`change`, (evt) => {
//   const { value } = filters.ascdesc;
//   console.log(value);
//   [`enres`, `jpres`].forEach((id) => {
//     const results = document.querySelector(`#${id}`);
//     Array.from(document.querySelectorAll(`.jpen-entry`))
//       .reverse()
//       .forEach((e) => {
//         results.appendChild(e);
//       });
//   });
// });

import express from "express";
import giongoDictionary from "./giongo.js";

let jpenDictionary, kanjiDictionary;

import("./JMdict_e.js").then((lib) => {
  jpenDictionary = new lib.JPENDictionary();
});

import("./Kanjidic2.js").then((lib) => {
  kanjiDictionary = new lib.KanjiDictionary();
});

const app = express();
app.disable('etag'); 
app.disable('x-powered-by');


const ensureSearchTerm = (req, res, next) => {
  const { term } = req.params;
  if (!term || term.trim() === ``) {
    return res.status(422).json({ error: `missing term` });
  }
  req.params.term = term.toLowerCase();
  next();
};

app.use(express.static(`public`));

app.get(`/`, (_, res) => res.redirect(`index.html`));

app.get(`/api/v1/jpen/:term?`, ensureSearchTerm, async (req, res) => {
  const { term } = req.params;
  const results = await jpenDictionary.find(term);
  res.json(results);
});

app.get(`/api/v1/kanji/:term?`, ensureSearchTerm, async (req, res) => {
  const { term } = req.params;
  const results = await kanjiDictionary.find(term);
  res.json(results);
});

app.get(`/api/v1/giongo/:term?`, ensureSearchTerm, async (req, res) => {
  const { term } = req.params;
  const { english, japanese } = giongoDictionary.find(term);

  const remap = (list) =>
    Object.entries(list).map(([key, value]) => ({
      reb: key,
      sense: Object.entries(value).map(([type, meaning]) => ({
        type,
        meaning,
      })),
    }));

  res.json({
    english: remap(english),
    japanese: remap(japanese),
  });
});

/*
term = `無残`;
results = await jpenDictionary.find(term);
logResults(term, results);

term = `いく`;
results = await jpenDictionary.find(term);
logResults(term, results);

term = `lion`;
results = await jpenDictionary.find(term);
logResults(term, results);

// Kanji

term = `deep`;
results = await kanjiDictionary.find(term, {}, KanjiDictionary.IS);
logResults(term, results);

term = convert(`minami`).hiragana;
results = await kanjiDictionary.find(term, {});
logResults(term, results);
*/

app.listen(3000, () => {
  console.log(`API server running`);
});

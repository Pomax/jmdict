import ime from "./public/ime.js";
import definitions from "./giongo-data.js";

const keys = Object.keys(definitions);

keys.forEach((hira) => {
  const conv = ime.convert(hira);
  definitions[hira].katakana = conv.katakana;
});

function searchMeaning(key, regex) {
  const entry = definitions[key];
  const cats = Object.keys(entry);
  let cat, meanings, meaning;
  for (let i = 0, last = cats.length; i < last; i++) {
    cat = cats[i];
    meanings = entry[cat];
    for (let j = 0, llast = meanings.length; j < llast; j++) {
      meaning = meanings[j];
      if (meaning.match(regex)) {
        return true;
      }
    }
  }
  return false;
}

const lookup = {
  get(key) {
    var res = JSON.parse(JSON.stringify(definitions[key]));
    delete res.katakana;
    return res;
  },

  find(term, filter = false) {
    const conv = ime.convert(term);

    const results = { english: [], japanese: [] };

    // japanese search
    if (conv.katakana) {
      const res = {},
        kana = conv.katakana;

      let term = kana,
        mterm = [`(^|\\b)`, term, `(\\b|$)`];
      if (term.startsWith(`*`)) {
        mterm.shift();
        term = term.substring(1);
      }
      if (term.endsWith(`*`)) {
        mterm.pop();
        term = term.substring(0, term.length - 1);
      }
      mterm = mterm.join(``).replaceAll(`*`, ``);
      let regex = new RegExp(mterm, `ui`);

      keys.forEach((key) => {
        if (definitions[key].katakana.match(regex)) {
          res[key] = lookup.get(key);
        }
      });
      results.japanese = res;
    }

    // english search
    if (conv === false) {
      let mterm = [`(^|\\b)`, term, `(\\b|$)`];
      if (term.startsWith(`*`)) {
        mterm.shift();
        term = term.substring(1);
      }
      if (term.endsWith(`*`)) {
        mterm.pop();
        term = term.substring(0, term.length - 1);
      }
      mterm = mterm.join(``).replaceAll(`*`, ``);
      let regex = new RegExp(mterm, `ui`);

      const res = {};
      keys.forEach((key) => {
        if (searchMeaning(key, regex)) {
          res[key] = lookup.get(key);
        }
      });
      results.english = res;
    }

    return results;
  },
};

export default lookup;

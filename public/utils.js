const sorting = {
  reading: (a, b) => {
    a = a.dataset.reb;
    b = b.dataset.reb;
    return a === b ? 0 : a < b ? -1 : 1;
  },
  romaji: (a, b) => {
    a = a.dataset.rom;
    b = b.dataset.rom;
    return a === b ? 0 : a < b ? -1 : 1;
  },
};

export default sorting;

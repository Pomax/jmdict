import { parseFile } from "./entry-parser.js";
import lookup from "./public/ime.js";
const { convert } = lookup;

/**
 * ...
 */
class JPENEntry {
  // <entry>
  //   <ent_seq>2212700</ent_seq>
  //   <k_ele>
  //     <keb>脱感作</keb>
  //   </k_ele>
  //   <r_ele>
  //     <reb>だつかんさ</reb>
  //   </r_ele>
  //   <r_ele>
  //     <reb>だっかんさ</reb>
  //   </r_ele>
  //   <sense>
  //     <pos>&n;</pos>
  //     <gloss>(immunological) desensitization</gloss>
  //   </sense>
  // </entry>

  constructor(data) {
    // copy over values
    Object.entries(data).forEach(([key, value]) => (this[key] = value));

    // fix the somewhat weird "not everything's nested the same way"
    // for keb, reb, and gloss entries.
    if (!this.r_ele) {
      this.r_ele = [];
    } else if (!this.r_ele.some) {
      this.r_ele = [this.r_ele];
    }

    if (!this.k_ele) {
      this.k_ele = [];
    } else if (!this.k_ele.some) {
      this.k_ele = [this.k_ele];
    }

    // ...English is a bit more work...
    if (!this.sense) {
      this.sense = [];
    } else if (this.sense.gloss) {
      this.sense = [this.sense];
    }

    this.sense = this.sense.map((s) =>
      Object.fromEntries(
        Object.entries(s).map(([key, val]) => {
          val = val.some ? val : [val];
          return [key, val];
        })
      )
    );
  }

  hasReading(regex, filters) {
    return this.r_ele.some((r) => r.reb.match(regex));
  }

  usesKanji(regex, filters) {
    return this.k_ele.some((k) => k.keb.match(regex));
  }

  hasTranslation(regex, filters) {
    return this.sense.some((v) => v.gloss.some((g) => `${g}`.match(regex)));
  }
}

/**
 * ...
 */
export class JPENDictionary {
  async find(term, filters = {}) {
    let mterm = [`(^|\\b)`, term, `(\\b|$)`];
    if (term.startsWith(`*`)) {
      mterm.shift();
      term = term.substring(1);
    }
    if (term.endsWith(`*`)) {
      mterm.pop();
      term = term.substring(0, term.length - 1);
    }
    mterm = mterm.join(``).replaceAll(`*`, ``);
    let regex = new RegExp(mterm, `ui`);

    const results = {
      english: [],
      japanese: [],
    };

    // Any latin? If so, search by translation.
    if (term.match(/[\u0020-\u1EFF]/)) {
      results.english = entries.filter((e) => e.hasTranslation(regex, filters));

      let ime = convert(term),
        hr = [],
        kr = [];
      if (ime.hiragana) {
        regex = new RegExp(mterm.replace(term, ime.hiragana), `ui`);
        hr = entries.filter((e) => e.hasReading(regex, filters));
      }
      if (ime.katakana) {
        regex = new RegExp(mterm.replace(term, ime.katakana), `ui`);
        kr = entries.filter((e) => e.hasReading(regex, filters));
      }
      results.japanese = hr.concat(kr);
    }

    // Any kanji? If so, search by kanji.
    if (term.match(/[\u3300-\u33FF\u3400-\u4DBF\u4E00-\u9FFF\uF900-\uFAFF]/)) {
      results.japanese = entries.filter((e) => e.usesKanji(regex, filters));
    }

    // Any kana? If so, search by reading.
    if (term.match(/[ぁ-んァ-ン]/u)) {
      results.japanese = entries.filter((e) => e.hasReading(regex, filters));
    }

    // No idea what this is...
    return results;
  }
}

console.log(`loading JPEN dictionary...`);
const JM_DICT = `JMdict_e.xml`;
const TAG = `entry`;
const start = Date.now();
const entries = await parseFile(JM_DICT, TAG, JPENEntry);
console.log(
  `loaded dictionary in ${((Date.now() - start) / 1000).toFixed(2)} seconds`
);

var config = localStorage.getItem("nrapiConfig") ? JSON.parse(localStorage["nrapiConfig"]) : {
  romaji: true,
  sorting: "kana",
  sortorder: "asc", // true = asc, false = desc
  revsort: false,  // true = reverse string sort, false=plain sort
  postype: "all"
};

(function bindPreferences() {
  var rom = document.querySelector("#romaji");
  if(config.romaji) { rom.setAttribute("checked", "checked"); }
  else { rom.checked = false; rom.removeAttribute("checked"); }

  var sorting = document.querySelector("#sorting");
  Array.prototype.slice.call(sorting.options).forEach(function(option, idx) {
    if(option.value !== config.sorting) {
      option.removeAttribute("selected");
    } else { option.setAttribute("selected", "selected"); }
  });

  var ascdesc = document.querySelector("#ascdesc");
  Array.prototype.slice.call(ascdesc.options).forEach(function(option, idx) {
    if(option.value !== config.sortorder) {
      option.removeAttribute("selected");
    } else { option.setAttribute("selected", "selected"); }
  });

  var postype = document.querySelector("#postype");
  Array.prototype.slice.call(postype.options).forEach(function(option, idx) {
    if(option.value !== config.postype) {
      option.removeAttribute("selected");
    } else { option.setAttribute("selected", "selected"); }
  });
}());


function getStrokeCount(kanji) {
  if(!kanji) return -1;
  return kanji.charCodeAt(0);
}

var sorting = {
  kana: function(a,b) {
    a = a.querySelector(".reb a").innerHTML;
    b = b.querySelector(".reb a").innerHTML;
    return a < b ? -1 : a > b ? 1 : 0;
  },
  kanji: function(a,b) {
    a = a.querySelector(".keb a");
    b = b.querySelector(".keb a");
    a = getStrokeCount(a ? a.innerHTML : false);
    b = getStrokeCount(b ? b.innerHTML : false);
    // we actually want to sort on stroke order first,
    // rather than just plain u icode ordering.
    return a < b ? -1 : a > b ? 1 : 0;
  },
  "relevance (en)": function(a,b) {
    var c = a.querySelector(".ranking"),
        d = b.querySelector(".ranking");
    if(!c || !d) return this.kana(a,b);
    a = parseFloat(c.value);
    b = parseFloat(d.value);
    return b - a;
  },
  nothing: function(a,b) {
    return 0;
  }
}

function rerender() {
  localStorage["nrapiConfig"] = JSON.stringify(config);

  var results = document.querySelector("#results");
  var children = Array.prototype.slice.call(document.querySelectorAll("#results > div"));

  // sort based on config
  var resorted = false;
  if(config.sorting) {
    resorted = true;
    children.sort(sorting[config.sorting]);
    if(config.sortorder === "desc") {
      children = children.reverse();
    }
  }

  children.forEach(function(child) {
    if (resorted) results.appendChild(child);
    // toggle styles based on config
    if(config.postype!=="all") {
      matched = false;
      Array.prototype.slice.call(child.querySelectorAll("input[class=pos]")).forEach(function(pos) {
        if(pos.value.indexOf(config.postype) > -1)
          matched = true;
      });
      child.style.display = matched ? "block" : "none";
    } else { child.style.display = "block"; }
  });

  // global style toggle
  Array.prototype.slice.call(document.querySelectorAll("rt")).forEach(function(rt) {
    if(config.romaji) { rt.classList.remove("hidden"); }
    else { rt.classList.add("hidden");  }
  });
}

function setRomaji(input) {
  config.romaji = !!input.checked;
  rerender();
}

function setSorting(select) {
  var option = select.options[select.selectedIndex].value;
  config.sorting = option;
  rerender();
}

function setSortOrder(select) {
  var option = select.options[select.selectedIndex].value;
  config.sortorder = option;
  rerender();
}

function setPosType(select) {
  var option = select.options[select.selectedIndex].value;
  config.postype = option;
  rerender();
}


// =============================


var open = function(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.onreadystatechange = function() {
    if (xhr.status == 200 && xhr.readyState == 4) {
      var data = xhr.responseText;
      try {
        data = JSON.parse(data);
        callback(data);
      } catch(e) {
        if(data.indexOf("</style>") > -1) {
          callback(data);
        } else { console.error("could not parse "+data); }
      }
    }
  };
  xhr.send(null);
}

var setInputValue = function(type, query) {
  var input = document.querySelector("formset." + type + " input[type=text]");
  if(!input) {
    console.log("formset." + type + " input[type=text]");
  }
  input.value = decodeURI(query);
};

var bindData = function(data) {
  var results = document.querySelector("#results");
  data = data.replace(/<hr>/g,'');

  if(data.indexOf("</div>") === -1) {
    data = "No results found. You can try adding wildcards (* and ? are supported) before or after your search term.";
  }

  results.innerHTML = data;
  rerender();

  // reroute <a> to JS links
  var as = results.querySelectorAll("a");
  as = Array.prototype.slice.call(as);
  as.forEach(function(a) {
    var r = a.href.split('/').slice(-3);
    a.href="?type="+r[0]+"&query="+r[2];
    a.onclick = function(evt) {
      linklookup(r[2], r[0], r[1]);
      evt.preventDefault;
      return false;
    };
  });

  // image redirection to api.nihongoresources.com
  var imgs = document.querySelectorAll(".entry img");
  imgs = Array.prototype.slice.call(imgs);
  imgs.forEach(function(img) {
    var newsrc = img.src.replace(location.protocol + "//" + location.host, '');
    img.src = "https://api.nihongoresources.com/" + newsrc;
  });

  // rebind styles
  var link = document.querySelector("link[href='/css/universal.css']");
  link.parentNode.removeChild(link);
  document.body.appendChild(link);
}

window.addEventListener("popstate", function(evt) {
  var state = evt.state;
  if(!state.typeid || state.typeid !== "nrapi-lookupdata") return;
  var data = state.data;
  bindData(data);
  setInputValue(state.type, state.query);
});

var showLoader = function() {
  var container = document.querySelector("#nrapi .loader");
  if(!container) return function(){};
  container.classList.remove("hidden");
  document.querySelector("#nrapi #filters").classList.add("hidden");
  var steps = ["", ".", "..", "..."],
      idx = -1,
      load = true,
      interval = 300,
      cycle = function() {
        idx = (idx + 1) % steps.length;
        container.innerHTML = steps[idx];
        if (load) setTimeout(cycle, interval);
      };
  cycle();
  // terminator function
  return function stopLoader() {
    load = false;
    document.querySelector("#nrapi #filters").classList.remove("hidden");
    container.classList.add("hidden");
  };
};

var lookups = function(input, type, method, samestate) {
  // we need a search term!
  if(input.value.trim() == "")
    return;

  //if we have one, let's search
  var url = "https://api.nihongoresources.com/" + type + "/" + method + "/" + input.value;
  var stopLoader = showLoader();
  open(url, function(data) {
    if (typeof data !== "string") {
      data = JSON.stringify(data,null,2).replace(/ /g,'&nbsp;').replace(/\n/g,'<br>');
    }
    stopLoader();
    bindData(data);

    if(!samestate) {
      url = "//www.nihongoresources.com/dictionaries/universal.html?type=" + type + "&query=" + input.value;
      history.pushState({
        "typeid": "nrapi-lookupdata",
        "type":   type,
        "method": method,
        "query":  input.value,
        "data":   data
      }, type + " search for " + input, url);
    }
  });
};

var linklookup = function(input, type, method) {
  document.querySelector('formset.'+type+' input[type=text]').value = decodeURI(input);
  lookups({value:input}, type, method);
}

var formsets = document.querySelectorAll("formset");
formsets = Array.prototype.slice.call(formsets);

formsets.forEach(function(set) {
  var type = set.getAttribute("class");

  var input = set.querySelector("input[type=text]");
  var json  = set.querySelector("input[value=json]");
  var html  = set.querySelector("input[value=html]");

  html.value = "search";

  var lookup = function(method) { lookups(input, type, method); };

  html.onclick    = function() { lookup("show"); };
  json.onclick    = function() { lookup("find"); };
  input.onkeydown = function(evt) { if(evt.which === 13 || evt.which === 10) { lookup("show"); }};

  var examples = set.querySelectorAll(".examples a");
  examples = Array.prototype.slice.call(examples);
  examples.forEach(function(a) {
    var span = document.createElement("span");
    span.innerHTML = decodeURI(a.innerHTML);
    span.onclick = function() {
      input.value = a.innerHTML;
      lookup("show");
    };
    a.parentNode.replaceChild(span,a);
  });
});

// now, is this a plain load, or a pageload with search parameters?
if(location.search) {
  var query = (function() {
    var query = {};
    location.search.substring(1).split('&').forEach(function(v) {
      v = v.split('=');
      query[v[0]] = v[1];
    });
    return query;
  }());

  if(query.type && query.query) {
    var input = document.querySelector("formset." + query.type + " input[type=text]");
    setInputValue(query.type, query.query);
    lookups(input, query.type, "show", true);
  }
} else {
  url = "//www.nihongoresources.com/dictionaries/universal.html";
  history.replaceState({ "type": "nrapi-lookupdata", "data": "" }, "", url);
}

document.querySelector("formset.dict input[type=text]").focus();

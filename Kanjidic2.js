import { parseFile } from "./entry-parser.js";
import lookup from "./public/ime.js";
const { convert } = lookup;

/**
 * ...
 */
class KanjiEntry {
  constructor(data) {
    // copy over values
    Object.entries(data).forEach(([key, value]) => (this[key] = value));

    // fix the somewhat weird "not everything's nested the same way".

    if (!this.reading_meaning) {
      this.reading_meaning = {};
    }

    if (!this.reading_meaning.rmgroup) {
      this.reading_meaning.rmgroup = {};
    }

    const rm = this.reading_meaning;

    if (typeof rm.rmgroup !== `object`) {
      rm.rmgroup = {
        reading: [],
        meaning: [],
      };
    }

    if (!rm.rmgroup.reading) {
      rm.rmgroup.reading = [];
    } else if (!rm.rmgroup.reading.some) {
      rm.rmgroup.reading = [rm.rmgroup.reading];
    }

    if (!rm.rmgroup.meaning) {
      rm.rmgroup.meaning = [];
    } else if (!rm.rmgroup.meaning.some) {
      rm.rmgroup.meaning = [rm.rmgroup.meaning];
    }

    if (!rm.nanori) {
      rm.nanori = [];
    } else if (!rm.nanori.some) {
      rm.nanori = [rm.nanori];
    }
  }

  is(kanji, filters) {
    return this.literal === kanji;
  }

  hasReading(regex, filters) {
    const { rmgroup, nanori } = this.reading_meaning;
    return (
      rmgroup.reading.some((r) => r.replaceAll(`.`, ``).match(regex)) ||
      (filters.nanori && nanori.some((r) => r.replaceAll(`.`, ``).match(regex)))
    );
  }

  hasTranslation(regex, filters) {
    const { meaning } = this.reading_meaning.rmgroup;
    return meaning.some((m) => `${m}`.match(regex));
  }
}

/**
 * ....
 */
export class KanjiDictionary {
  async find(term, filters = {}) {
    let mterm = [`(^|\\b)`, term, `(\\b|$)`];
    if (term.startsWith(`*`)) {
      mterm.shift();
      term = term.substring(1);
    }
    if (term.endsWith(`*`)) {
      mterm.pop();
      term = term.substring(0, term.length - 1);
    }
    mterm = mterm.join(``).replaceAll(`*`, ``);
    let regex = new RegExp(mterm, `ui`);

    const results = { english: [], japanese: [] };

    // Any latin? If so, search by translation.
    if (term.match(/[\u0020-\u1EFF]/)) {
      results.english = entries.filter((e) => e.hasTranslation(regex, filters));

      let ime = convert(term),
        hr = [],
        kr = [];
      if (ime.hiragana) {
        regex = new RegExp(mterm.replace(term, ime.hiragana), `ui`);
        hr = entries.filter((e) => e.hasReading(regex, filters));
      }
      if (ime.katakana) {
        regex = new RegExp(mterm.replace(term, ime.katakana), `ui`);
        kr = entries.filter((e) => e.hasReading(regex, filters));
      }
      results.japanese = hr.concat(kr);
    }

    // Just kanji? If so, this is a literal search.
    if (term.match(/[\u3300-\u33FF\u3400-\u4DBF\u4E00-\u9FFF\uF900-\uFAFF]/)) {
      results.japanese = entries.filter((e) => e.is(term, filters));
    }

    // Any kana? If so, search by reading.
    if (term.match(/[ぁ-んァ-ン]/u)) {
      results.japanese = entries.filter((e) => e.hasReading(regex, filters));
    }

    return results;
  }
}

console.log(`loading kanji dictionary...`);
const KANJIDIC2 = `Kanjidic2.xml`;
const TAG = `character`;
const start = Date.now();
const entries = await parseFile(KANJIDIC2, TAG, KanjiEntry);
console.log(
  `loaded kanji in ${((Date.now() - start) / 1000).toFixed(2)} seconds`
);

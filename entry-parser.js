import fs from "fs";
import { XMLParser } from "fast-xml-parser";

const parser = new XMLParser();

export function parseFile(filepath, tag, EntryClass) {
  return new Promise((resolve) => {
    let entries = [];
    const prefix = `<${tag}>`;
    const separator = `</${tag}>`;
    const readableStream = fs.createReadStream(filepath);

    readableStream.on("error", function (error) {
      console.log(`error: ${error.message}`);
    });

    readableStream.on("data", (chunk) => {
      const data = chunk.toString("utf-8").replaceAll(`\n`, ` `);
      let parts = data.split(separator);
      parts = parts.map((v, pos) =>
        pos === parts.length - 1 ? v : (v + separator).trim()
      );

      // make sure to join up across chunks
      if (entries.length > 0) {
        const last = entries.pop();
        const first = parts.shift();
        let updated = (last + first).split(separator).filter(Boolean);
        updated = updated.map((v) => (v + separator).trim());
        entries.push(...updated);
      }

      entries.push(...parts);
    });

    readableStream.on("close", function (error) {
      // final cleanup
      entries.pop();
      entries.unshift(prefix + entries.shift().split(prefix)[1]);
      const parsed = entries.map((entry) => {
        let obj;
        try {
          obj = parser.parse(entry);
        } catch (e) {
          console.error(entry);
          throw e;
        }
        return obj[tag];
      });
      resolve(parsed.map((obj) => new EntryClass(obj)));
    });
  });
}
